const express = require('express')

const app = express()

const MongoClient = require('mongodb').MongoClient

const url = 'mongodb://localhost:27017'
const dbName = 'my-spotify-db'

let db

MongoClient.connect(url, function (err, client) {
  console.log('Connected successfully to server')
  db = client.db(dbName)
})

app.use(express.json())

app.get('/ten-short-range-rankings', (req, res) => {
  db.collection('rankings')
    .find({})
    .toArray(function (err, docs) {
      if (err) {
        console.log(err)
        throw err
      }
      res.status(200).json(docs)
    })
})

app.get('/ten-short-range-rankings/:id', async (req, res) => {
  res.header('Access-Control-Allow-Origin', '*')
  const id = req.params.id
  try {
    const docs = await db.collection('ten-short-range').find({ id }).toArray()
    res.status(200).json(docs[0].rankings[docs[0].rankings.length - 1])
  } catch (err) {
    console.log(err)
    throw err
  }
})

app.put('/ten-short-range-rankings/:id', async (req, res) => {
  const id = req.params.id
  const rankingData = req.body
  try {
    await db.collection('ten-short-range').updateOne(
      { id },
      {
        $push: {
          rankings: rankingData
        }
      }
    )
    res.status(200).json('Success')
  } catch (err) {
    console.log(err)
    throw err
  }
})

app.listen(8080, () => {
  console.log('Server listening')
})
