# my-spotify-api

This should help get you started developing Express server and NodeJS.

## Project Setup

You will need to install MongoDB.

Create a database named my-spotify-db.

Create collection named ten-short-range.

```sh
npm install
```

### Start server

```sh
npm run dev
```
